#ifndef MANDEL_CUDA_H
#define MANDEL_CUDA_H

#include "main.h"
#include <thrust/complex.h>
#include <png.h>

png_byte** create_pixel_matrix_cuda(thrust::complex<float> c0, float delta_x, float delta_y, int width,
	struct height_range my_range, int num_threads, std::string filename);
void destroy_img_cuda(png_byte** img, int height);

#endif
