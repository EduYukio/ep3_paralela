#include <iostream>
#include <string>
#include <thrust/complex.h>
#include <png.h>
#include "main.h"

using thrust::complex;

// https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
#define gpu_error_check(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
	if (code != cudaSuccess) {
		fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) {
			exit(code);
		}
	}
}

__device__
float norma_cuda(const complex<float>& c)
{
	return sqrt(c.real() * c.real() + c.imag() * c.imag());
}

__device__
bool is_mandelbrot_cuda(complex<float> c, int& count)
{
	if (count >= M) {
		return true;
	}
	if (norma_cuda(c) <= 2) {
		{
			complex<float> old_c{c};
			c *= c;
			c += old_c;
		}
		return is_mandelbrot_cuda(c, ++count);
	}
	return false;
}

__device__
void write_pixel_cuda(const complex<float>& c, png_byte** img, int i, int j)
{
	int count = 1;
	/* a cor se aproxima de preto à medida que count se aproxima de M. */
	img[i][j] = is_mandelbrot_cuda(c, count)
		? 0x00
		: static_cast<png_byte>(0xff - (static_cast<float>(count) / M) * 0xc0);
}

__global__
// pass c0 by value (don't copy references from the CPU)
void write_all_pixels(complex<float> c0, float delta_x, float delta_y,
	int width, struct height_range my_range, png_byte **img)
{
	int delta_height = my_range.h_final - my_range.h_init;

	for (int i = blockIdx.x; i <= delta_height; i += gridDim.x) {
		for (int j = threadIdx.x; j <= width; j += blockDim.x) {
			complex<float> c{c0.real() + j * delta_x, c0.imag() + (i + my_range.h_init) * delta_y};
			write_pixel_cuda(c, img, i, j);
		}
	}
}

png_byte** create_pixel_matrix_cuda(complex<float> c0, float delta_x, float delta_y,
	int width, struct height_range my_range, int num_threads, std::string filename)
{
	png_byte **img = NULL;

	int delta_height = my_range.h_final - my_range.h_init;

	gpu_error_check(cudaMallocManaged(&img, (delta_height + 1) * sizeof(png_byte *)));
	for (int k = 0; k <= delta_height; k++) {
		gpu_error_check(cudaMallocManaged(&img[k], (width + 1) * sizeof(png_byte)));
	}

	write_all_pixels<<<delta_height + 1, num_threads>>>(c0, delta_x, delta_y, width, my_range, img);
	gpu_error_check(cudaDeviceSynchronize());
	return img;
}

void destroy_img_cuda(png_byte** img, int height)
{
	for (int k = 0; k <= height; k++) {
		gpu_error_check(cudaFree(img[k]));
	}
	gpu_error_check(cudaFree(img));
}
