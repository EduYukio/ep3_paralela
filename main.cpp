#include <iostream>
#include <cstdlib>
#include <thrust/complex.h>
#include <cmath>
#include <type_traits>
#include <string>
#include <assert.h>
#include <png.h>
#include <setjmp.h>
#include <mpi.h>
#include "main.h"
#include "mandelomp.h"
#include "mandelcuda.h"

#define TAG_MY_IMG 0
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

using thrust::complex;

inline void die(const char* msg)
{
        fprintf(stderr, "%s\n", msg);
	MPI_Finalize();
	exit(EXIT_FAILURE);
}

MPI_Datatype declare_task_type()
{
    int err = 0;
    MPI_Datatype type;

    MPI_Datatype internal_types[] = {MPI_INT, MPI_INT};
    int types_len[] = {1, 1};
    int num_elements = ARRAY_SIZE(internal_types);

    MPI_Aint offsets[] = {
        offsetof(struct height_range, h_init),
        offsetof(struct height_range, h_final)
    };

    assert(offsets[num_elements - 1] + sizeof(int) == sizeof(struct height_range));

    err |= MPI_Type_create_struct(
	    num_elements,
            types_len,
            offsets,
            internal_types,
            &type
    );
    err |= MPI_Type_commit(&type);

    if (err) { die("There was a MPI type registration failure.\n"); }

    return type;
}



void write_image(const std::string& filename, int width, int height, png_byte **img)
{
	FILE *f = fopen(filename.c_str(), "wb");
	if (!f) {
		std::cout << "failed to open file " << filename << std::endl;
		return;
	}

	png_struct *png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) {
		std::cout << "failed to create png_ptr" << std::endl;
		fclose(f);
		exit(EXIT_FAILURE);
	}
	png_info *info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		std::cout << "failed to create info_ptr struct" << std::endl;
		goto end;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		std::cout << "libpng internal failure" << std::endl;
		goto end;
	}
	png_init_io(png_ptr, f);
	png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_GRAY,
		PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	png_write_info(png_ptr, info_ptr);
	png_write_image(png_ptr, img);
	png_write_end(png_ptr, NULL);
end:
	png_destroy_write_struct(&png_ptr, &info_ptr);
	fclose(f);
}

void usage(void)
{
	std::cout << "uso: mbrot <C0_REAL> <C0_IMAG> <C1_REAL> <C1_IMAG> <W> <H> <CPU/GPU> <THREADS> <SAIDA>"
		<< std::endl;
}

void get_image(int width, int height, int num_proc,
	const std::string& filename, struct height_range* ranges)
{
	png_byte** img = static_cast<png_byte**>(calloc(height + 1, sizeof(png_byte*)));
	for (int i = 0; i < height + 1; ++i) {
		img[i] = static_cast<png_byte*>(calloc(width + 1, sizeof(png_byte)));
	}

	int* index_array = static_cast<int*>(calloc(num_proc, sizeof(int)));
	MPI_Request* requests = static_cast<MPI_Request*>(calloc(num_proc, sizeof(MPI_Request)));

	int finished_rank = 0;

	for (int i = 0; i < num_proc; ++i) {
		MPI_Irecv(img[ranges[i].h_init], width + 1, MPI_CHAR, i, TAG_MY_IMG, MPI_COMM_WORLD, &requests[i]);
	}

	for (int i = 0; i < height + 1; ++i) {
		MPI_Waitany(num_proc, requests, &finished_rank, MPI_STATUS_IGNORE);

		int delta_h = ranges[finished_rank].h_final - ranges[finished_rank].h_init;
		if (++index_array[finished_rank] <= delta_h) {
			MPI_Irecv(img[ranges[finished_rank].h_init + index_array[finished_rank]],
				width + 1, MPI_CHAR, finished_rank, TAG_MY_IMG, MPI_COMM_WORLD,
				&requests[finished_rank]);
		} else {
			requests[finished_rank] = MPI_REQUEST_NULL;
		}
	}

	free(index_array);
	free(requests);
	index_array = NULL;
	requests = NULL;

	write_image(filename, width, height, img);

	for (int i = 0; i < height + 1; ++i) {
		free(img[i]);
	}
	free(img);
}

struct height_range* calculate_ranges(int num_proc, int height)
{
	struct height_range* ranges = static_cast<struct height_range*>(
		calloc(num_proc, sizeof(struct height_range)));

	ranges[0].h_init = 0;
	for (int i = 0; i < num_proc; ++i) {
		if (i > 0) {
			ranges[i].h_init = ranges[i - 1].h_final + 1;
		}
		/*
		height_final += (height + 1) / num_proc;
		if (rank < (height + 1) % num_proc) {
			height_final++;
		}
		*/

		ranges[i].h_final = ranges[i].h_init + (height + 1) / num_proc;
		if (i >= (height + 1) % num_proc) {
			ranges[i].h_final--;
		}
	}
	return ranges;
}

int main(int argc, char **argv)
{
	if (argc != 10) {
		usage();
		return -1;
	}

	float c0_real = std::stof(std::string{argv[1]});
	float c0_imag = std::stof(std::string{argv[2]});
	float c1_real = std::stof(std::string{argv[3]});
	float c1_imag = std::stof(std::string{argv[4]});
	const int width = std::stoi(std::string{argv[5]});
	const int height = std::stoi(std::string{argv[6]});
	std::string cpu_or_gpu{argv[7]};
	int num_threads = std::stoi(std::string{argv[8]});
	std::string filename{argv[9]};

	bool is_gpu = false;
	if (cpu_or_gpu == std::string("cpu")) {
		is_gpu = false;
	} else if (cpu_or_gpu == std::string("gpu")) {
		is_gpu = true;
	} else {
		usage();
		return -1;
	}

	MPI_Init(&argc, &argv);
	int num_proc;
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Datatype mpi_height_range = declare_task_type();

	struct height_range my_range;
	struct height_range *ranges = NULL;

	if (rank == 0) {
		ranges = calculate_ranges(num_proc, height);
	}

	int err = 0;
	err |= MPI_Scatter(
		ranges,
		1,
		mpi_height_range,
		&my_range,
		1,
		mpi_height_range,
		0,
		MPI_COMM_WORLD
	);
	if (err) { die("MPI_Scatter() failed."); }

	complex<float> c0{c0_real, c0_imag}, c1{c1_real, c1_imag};
	const float delta_x = (c1.real() - c0.real()) / width;
	const float delta_y = (c1.imag() - c0.imag()) / height;

	png_byte **my_img = is_gpu
		? create_pixel_matrix_cuda(c0, delta_x, delta_y, width, my_range, num_threads, filename)
		: create_pixel_matrix_omp(c0, delta_x, delta_y, width, my_range, num_threads, filename);

	for (int i = 0; i < my_range.h_final - my_range.h_init + 1; ++i) {
		err |= MPI_Send(my_img[i], width + 1, MPI_CHAR, 0, TAG_MY_IMG, MPI_COMM_WORLD);
		if (err) { die("MPI_Send() failed."); }
	}

	if (rank == 0) {
		get_image(width, height, num_proc, filename, ranges);

		free(ranges);
		ranges = NULL;
	}

	int delta_height = my_range.h_final - my_range.h_init;
	if (is_gpu) {
		destroy_img_cuda(my_img, delta_height);
	} else {
		for (int i = 0; i <= delta_height; ++i) {
			free(my_img[i]);
		}
		free(my_img);
	}

	MPI_Finalize();
}
