CXX ?= c++
CUDAC ?= nvcc
MPICXX ?= mpic++
CXXFLAGS += $(shell pkg-config --cflags libpng) $(shell pkg-config --cflags mpi) -I/usr/local/cuda/include/ -fopenmp -g
CUDA_FLAGS += -g
LDLIBS += $(shell pkg-config --libs-only-l libpng) -lmpi_cxx -lcuda -lcudart -lcudadevrt -fopenmp
LDFLAGS += $(shell pkg-config --libs-only-L libpng) -L/usr/local/cuda/lib64
CXX_OBJ := mandelomp.o
MPIC_OBJ := main.o
CUDA_OBJ := mandelcuda.o
BIN := dmbrot
LINK_OBJ := link.o

$(BIN): $(CXX_OBJ) $(CUDA_OBJ) $(LINK_OBJ) $(MPIC_OBJ)
	$(MPICXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(CUDA_OBJ): %.o: %.cu
	$(CUDAC) --device-c $(CUDA_FLAGS) -o $@ $<

$(LINK_OBJ) : $(CUDA_OBJ)
	$(CUDAC) --device-link $(CUDA_FLAGS) -o $@ $^

$(MPIC_OBJ): %.o : %.cpp
	$(MPICXX) -c $(CXXFLAGS) -o $@ $<

$(CXX_OBJ): %.o : %.cpp
	$(CXX) -c $(CXXFLAGS) -o $@ $<

clean:
	rm -f $(BIN) $(CXX_OBJ) $(CUDA_OBJ) $(LINK_OBJ)
