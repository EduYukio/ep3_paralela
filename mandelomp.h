#ifndef MANDEL_OMP_H
#define MANDEL_OMP_H

#include <string>
#include <thrust/complex.h>
#include <png.h>
#include "main.h"

png_byte** create_pixel_matrix_omp(thrust::complex<float> c0, float delta_x, float delta_y, int width,
	struct height_range my_range, int num_threads, std::string filename);

#endif
